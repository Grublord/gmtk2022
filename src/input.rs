use super::*;

pub(crate) fn clear_input(mut output: ResMut<Input<knife::Knife>>) {
    output.clear();
}

pub(crate) fn trigger_cut_keyboard(
    input: Res<Input<KeyCode>>,
    mut output: ResMut<Input<knife::Knife>>,
) {
    for i in knife::Knife::all() {
        if input.any_just_pressed(i.keys()) {
            output.press(i);
        }

        if input.any_just_released(i.keys()) && !input.any_pressed(i.keys()) {
            output.release(i);
        }
    }
}

pub(crate) fn trigger_cut_touch(
    windows: Res<Windows>,
    input: Res<Touches>,
    mut output: ResMut<Input<knife::Knife>>,
) {
    let window = windows.get_primary().unwrap();

    let knife = |finger: &bevy::input::touch::Touch| {
        if finger.start_position().x < window.width() / 2. {
            knife::Knife::Left
        } else {
            knife::Knife::Right
        }
    };

    for finger in input.iter_just_pressed() {
        output.press(knife(finger));
    }

    for finger in input
        .iter_just_released()
        .chain(input.iter_just_cancelled())
    {
        output.release(knife(finger));
    }
}
