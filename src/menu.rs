use super::*;

pub(crate) fn start_menu(
    commands: &mut Commands,
    assets: &assets::GlobalAssets,
    progress: &mut GameState,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(1.2 * Vec2::new(2., 2. * BG_ASPECT_YDX) * WIDTH),
                ..Default::default()
            },
            texture: assets.menu.clone(),
            transform: Transform::default().with_translation(Vec3::new(0., 0., 0.)),
            ..Default::default()
        })
        .insert(OfState);

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(0.7 * Vec2::new(2., 2. * LOGO_ASPECT_YDX) * WIDTH),
                ..Default::default()
            },
            texture: assets.logo.clone(),
            transform: Transform::default().with_translation(Vec3::new(0., 0., 1.)),
            ..Default::default()
        })
        .insert(OfState);

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                "Click to continue",
                assets.text_style.clone(),
                bevy::text::TextAlignment {
                    vertical: VerticalAlign::Center,
                    horizontal: HorizontalAlign::Center,
                },
            ),
            transform: Transform::default()
                .with_translation(Vec3::new(0., -0.2 * WIDTH, 1.))
                .with_scale(Vec3::new(0.1, 0.1, 1.)),
            ..Default::default()
        })
        .insert(OfState);

    *progress = GameState::Menu;
}

pub(crate) fn start_game(
    input_key: Res<Input<KeyCode>>,
    input_mouse: Res<Input<MouseButton>>,
    input_touch: Res<Touches>,
    mut commands: Commands,
    previous_entities: Query<Entity, With<OfState>>,
    mut music: ResMut<Option<Music>>,
    audio_assets: Res<Assets<bevy::audio::AudioSink>>,
    assets: Res<assets::GlobalAssets>,
    audio: Res<Audio>,
    mut state: ResMut<GameState>,
) {
    let handle = match *state {
        GameState::Menu => true,
        GameState::StageProgress { .. } => false,
        GameState::ScoreScreen(start) => start.elapsed() >= FORCE_SCORE_FOR,
    };

    if !handle {
        return;
    }

    macro_rules! input_end {
        (Touches $x:expr) => {
            input_touch.iter_just_released().next().is_some()
                && input_touch
                    .iter()
                    .all(|i| input_touch.get_pressed(i.id()).is_none())
        };
        ($x:expr) => {
            $x.get_just_released().next().is_some() && $x.get_pressed().next().is_none()
        };
    }

    if input_end!(input_key) || input_end!(Touches input_touch) || input_end!(input_mouse) {
        state::clean(&mut commands, &previous_entities, &mut music, &audio_assets);

        super::start_game(
            &mut commands,
            &assets,
            &audio,
            &mut state,
            &mut music,
            &audio_assets,
        );
    }
}
