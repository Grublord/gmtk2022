use bevy::prelude::*;
use bevy::sprite::Anchor;
use bevy::utils::Instant;
use std::time::Duration;
use std::f32::consts;

const WIDTH: f32 = 128.;
const RADIUS: f32 = 0.5;
const BPM: f32 = 95.;
const BPS: f32 = BPM / 60.;

const KNIFE_UP_ASPECT_XDY: f32 = 228. / 1025.;
const KNIFE_UP_FACT: f32 = 1.2;

const BG_ASPECT_YDX: f32 = 1080. / 1920.;
const LOGO_ASPECT_YDX: f32 = 971. / 3645.;

const CUT_ASSET_ASPECT_XDY: f32 = 800. / 500.;
const CUT_ASPECT_XDY: f32 = 2440. / 236.;

const WAIT_BEATS: f32 = 1. * 4.;
const BEATS_PER_CHART: f32 = 3. * 4.;

const BEAT_COUNT: f32 = 80.;

const MAX_ANGLE: f32 = 7. / 8. * consts::TAU;

const FORCE_SCORE_FOR: Duration = Duration::from_millis(500);

mod state;
use state::*;
mod assets;
mod cuts;
mod input;
mod knife;
mod menu;
mod score;
mod turntable;
mod lives;

fn switch_time() -> Duration {
    Duration::from_secs_f32(WAIT_BEATS / BPS)
}

fn rotation_time() -> Duration {
    Duration::from_secs_f32(BEATS_PER_CHART / BPS)
}

fn cycle_time() -> Duration {
    switch_time() + rotation_time()
}

fn song_time() -> Duration {
    Duration::from_secs_f32(BEAT_COUNT / BPS)
}

fn rotation(amount: f32) -> Quat {
    Quat::from_rotation_z(amount)
}

struct StageConfig {
    cut_max_diff: f32,
}

struct Music(Handle<bevy::audio::AudioSink>);

struct GameOver {
    stop_music: bool,
}

fn switch_chart(
    mut commands: Commands,
    turntable: Query<Entity, With<turntable::Turntable>>,
    mut cuts: Query<(&mut cuts::Cut, &Parent)>,
    assets: Res<assets::GlobalAssets>,
    mut state: ResMut<GameState>,
) {
    let (past_rotations, last_start, start) = in_state!(
        StageProgress { past_rotations, last_start, start, .. } = &mut *state
            => (past_rotations, last_start, start)
    );

    let switch_time = switch_time();
    let chart_time = switch_time + rotation_time();
    let mut finshed_rotation = false;
    while last_start.elapsed() >= chart_time {
        *past_rotations += 1;
        *last_start += chart_time;
        finshed_rotation = true;
    }

    if finshed_rotation {
        for i in turntable.iter() {
            commands
                .entity(i)
                .insert(turntable::TurntableLeave { from: *last_start });

            for mut j in cuts
                .iter_mut()
                .filter(|(_, parent)| parent.0 == i)
                .map(|(cut, _)| cut)
            {
                j.active = false;
            }
        }

        if start.elapsed() + cycle_time() <= song_time() {
            turntable::spawn_turntable(&mut commands, *last_start, &assets, *past_rotations);
        }
    }
}

fn start_game(
    commands: &mut Commands,
    assets: &assets::GlobalAssets,
    audio: &Audio,
    progress: &mut GameState,
    music: &mut Option<Music>,
    audio_assets: &Assets<bevy::audio::AudioSink>,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(1.2 * Vec2::new(2., 2. * BG_ASPECT_YDX) * WIDTH),
                ..Default::default()
            },
            texture: assets.bg.clone(),
            transform: Transform::default().with_translation(Vec3::new(0., 0., 0.)),
            ..Default::default()
        })
        .insert(OfState);

    commands.spawn_bundle(score::ScoreDisplayBundle::new(assets));

    for knife in knife::Knife::all() {
        commands.spawn_bundle(knife::KnifeBundle::for_(knife, assets));
    }

    *music = Some(Music(
        audio_assets.get_handle(audio.play(assets.music.clone())),
    ));
    let now_music = Instant::now();
    let now_logic = now_music - switch_time();
    *progress = GameState::StageProgress {
        past_rotations: 0,
        last_start: now_logic,
        score: 0,
        lives: 3,
        start: now_music,
    };

    turntable::spawn_turntable(commands, now_logic, &assets, 0);

    for i in 0..3 {
        commands.spawn_bundle(lives::HeartBundle::new(assets, i));
    }
}

fn game_over_on_music_end(state: Res<GameState>, mut output: EventWriter<GameOver>) {
    if in_state!(StageProgress { start, .. } = *state => start).elapsed() >= song_time() {
        output.send(GameOver { stop_music: false });
    }
}

fn handle_game_over(
    mut commands: Commands,
    assets: Res<assets::GlobalAssets>,
    mut state: ResMut<GameState>,
    mut event: EventReader<GameOver>,
    previous_entities: Query<Entity, With<OfState>>,
    audio_assets: Res<Assets<bevy::audio::AudioSink>>,
    mut music: ResMut<Option<Music>>,
) {
    if let Some(stop_music) = event.iter().map(|i| i.stop_music).reduce(|l, r| l || r) {
        // consume all
        let score = in_state!(StageProgress { score, .. } = *state => score);

        let mut _none = None;
        let music = if stop_music { &mut music } else { &mut _none };
        state::clean(&mut commands, &previous_entities, music, &audio_assets);

        score::game_over(&mut commands, &assets, &mut state, score);
    }
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>, mut progress: ResMut<GameState>) {
    let assets = assets::GlobalAssets::load(&asset_server);

    let mut camera = OrthographicCameraBundle::new_2d();
    camera.orthographic_projection.scaling_mode =
        bevy::render::camera::ScalingMode::FixedHorizontal;
    camera.orthographic_projection.scale = WIDTH;
    commands.spawn_bundle(camera);

    menu::start_menu(&mut commands, &assets, &mut progress);

    commands.insert_resource(assets);
}

fn main() {
    let mut app = App::new();
    app.add_plugins(DefaultPlugins)
        .insert_resource(ClearColor(Color::WHITE))
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(GameState::Menu)
        .insert_resource(StageConfig {
            cut_max_diff: 0.025,
        })
        .insert_resource(<Input<knife::Knife>>::default())
        .insert_resource(None::<Music>)
        .add_event::<GameOver>()
        .add_system(menu::start_game)
        .add_system(
            input::clear_input
                .before(input::trigger_cut_keyboard)
                .before(input::trigger_cut_touch),
        )
        .add_system(input::trigger_cut_keyboard)
        .add_system(input::trigger_cut_touch)
        .add_system(
            cuts::do_cut
                .after(input::trigger_cut_keyboard)
                .after(input::trigger_cut_touch)
                .before(handle_game_over),
        )
        .add_system(
            knife::cut_visuals
                .after(input::trigger_cut_keyboard)
                .after(input::trigger_cut_touch),
        )
        .add_system(turntable::success_image.after(cuts::do_cut))
        .add_system(switch_chart.after(cuts::do_cut))
        .add_system(turntable::turn_turntable.before(switch_chart))
        .add_system(turntable::enter)
        .add_system(turntable::leave)
        .add_system(score::update_score)
        .add_system(game_over_on_music_end.before(handle_game_over))
        .add_system(handle_game_over)
        .add_system(lives::update_hearts)
        .add_system(bevy::input::system::exit_on_esc_system)
        .add_startup_system(setup);

    #[cfg(target_arch = "wasm32")]
    {
        app.add_plugin(bevy_web_resizer::Plugin);
    }

    app.run();
}
