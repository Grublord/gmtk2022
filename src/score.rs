use super::*;
use bevy::text::Text;

#[derive(Component)]
pub(crate) struct ScoreDisplay;

#[derive(Bundle)]
pub(crate) struct ScoreDisplayBundle {
    #[bundle]
    text: Text2dBundle,
    of_state: OfState,
    score_display: ScoreDisplay,
}

impl ScoreDisplayBundle {
    pub(crate) fn new(assets: &assets::GlobalAssets) -> Self {
        ScoreDisplayBundle {
            text: Text2dBundle {
                text: Text::with_section(
                    "0",
                    assets.text_style.clone(),
                    bevy::text::TextAlignment {
                        vertical: VerticalAlign::Bottom,
                        horizontal: HorizontalAlign::Right,
                    },
                ),
                transform: Transform::from_translation(Vec3::new(0.9 * WIDTH, 0., 1.))
                    .with_scale(Vec3::new(0.1, 0.1, 1.)),
                ..Default::default()
            },
            of_state: OfState,
            score_display: ScoreDisplay,
        }
    }
}

pub(crate) fn update_score(
    state: Res<GameState>,
    mut display: Query<&mut Text, With<ScoreDisplay>>,
) {
    let score = in_state!(StageProgress { score, .. } = *state => score);
    for mut i in display.iter_mut() {
        i.sections[0].value = format!("{score}");
    }
}

pub(crate) fn game_over(
    commands: &mut Commands,
    assets: &assets::GlobalAssets,
    state: &mut GameState,
    score: usize,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(1.2 * Vec2::new(2., 2. * BG_ASPECT_YDX) * WIDTH),
                ..Default::default()
            },
            texture: assets.menu.clone(),
            transform: Transform::default().with_translation(Vec3::new(0., 0., 0.)),
            ..Default::default()
        })
        .insert(OfState);

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                &format!("Your score: {score}"),
                assets.text_style.clone(),
                bevy::text::TextAlignment {
                    vertical: VerticalAlign::Center,
                    horizontal: HorizontalAlign::Center,
                },
            ),
            transform: Transform::default()
                .with_translation(Vec3::new(0., 0., 1.))
                .with_scale(Vec3::new(0.2, 0.2, 1.)),
            ..Default::default()
        })
        .insert(OfState);

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                "Press any key to try again",
                assets.text_style.clone(),
                bevy::text::TextAlignment {
                    vertical: VerticalAlign::Center,
                    horizontal: HorizontalAlign::Center,
                },
            ),
            transform: Transform::default()
                .with_translation(Vec3::new(0., -0.2 * WIDTH, 1.))
                .with_scale(Vec3::new(0.1, 0.1, 1.)),
            ..Default::default()
        })
        .insert(OfState);

    *state = state::GameState::ScoreScreen(Instant::now());
}
